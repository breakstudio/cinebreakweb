/**
 * Created by Kevin on 08/09/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:MovieDetailsController
 * @description
 * # MovieDetailsController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('MovieDetailsController', ['$scope', '$rootScope','$state', '$stateParams', 'commentFactory', 'AuthFactory',
    function ($scope,$rootScope, $state, $stateParams, commentFactory, AuthFactory) {

      //Enable the admin operations
      $scope.isAdmin = AuthFactory.isAdmin();

      $rootScope.$on('login:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('logout:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('registration:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });


      // operations for the comments
      $scope.alerts = [];

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };

      $scope.movieId = $stateParams.id;
      $scope.movie = $stateParams.movie;

      $scope.newComment = {
        rating: 5,
        comment: ""
      };

      $scope.submitComment = function () {
        //var userInfo = $localStorage.getObject('userinfo', '{}');


        if (!AuthFactory.isAuthenticated ) {
          //$scope.alerts = [];
          $scope.alerts.push({type: 'alert', msg: 'You need to be logged in before commenting a movie'});
          return;
        }


        commentFactory.save({id: $stateParams.id}, $scope.newComment,
          function (response) {
            $scope.movie = response;

            $scope.commentForm.$setPristine();
            $scope.newComment = {
              rating: 5,
              comment: ""
            };
          },
          function (response) {
            $scope.alerts.push({type: 'alert', msg: "Error: " + response.status + " " + response.statusText});
            console.log("Error: " + response.status + " " + response.statusText);
          });



      }

    }]);
