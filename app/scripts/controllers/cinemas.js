'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:CinemasController
 * @description
 * # HomeController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('CinemasController', ['$scope', '$rootScope', '$state', '$stateParams', 'CinemasFactory', 'AuthFactory',
    function ($scope, $rootScope, $state, $stateParams, CinemasFactory, AuthFactory) {
      $scope.cinemas = [];

      //operations for admins
      $scope.isAdmin = AuthFactory.isAdmin();

      $rootScope.$on('login:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('logout:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('registration:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });

      if ($stateParams.city) {

        CinemasFactory.query({'city.abbreviation': $stateParams.city},
          function (response) {
            $scope.cinemas = response;
          },
          function (response) {
            console.log("Error: " + response.status + " " + response.statusText);
          });
      }

      $scope.viewSpecificCinema = function (selectedCinema) {
        $state.go('cineBreak.specificCinema', {id: selectedCinema._id, cinema: selectedCinema});
      }

    }]);
