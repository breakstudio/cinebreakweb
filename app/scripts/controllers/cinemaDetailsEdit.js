/**
 * Created by Kevin on 08/09/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:CinemaDetailsEditController
 * @description
 * # MovieDetailsController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('CinemaDetailsEditController', ['$scope', '$state', '$stateParams', 'CinemasFactory',
    function ($scope, $state, $stateParams, CinemasFactory) {

      //getting the information of the cinema
      $scope.isNew = false;
      if ($stateParams.cinema) {
        $scope.cinemaData = $stateParams.cinema;
      }
      else {//in case of new cinema
        $scope.cinemaData = {prices: []};
        $scope.isNew = true;
      }

      //operations for the prices collection
      $scope.pricesToAdd = [{
        format: '',
        description: '',
        price: ''
      }];
      $scope.addNewPriceToPricesArray = function (priceToAdd) {
        var index = $scope.pricesToAdd.indexOf(priceToAdd);
        $scope.pricesToAdd.splice(index, 1);
        $scope.cinemaData.prices.push(angular.copy(priceToAdd))
      };

      $scope.removepriceFromPricesArray = function (priceToRemove) {
        var index = $scope.cinemaData.prices.indexOf(priceToRemove);
        $scope.cinemaData.prices.splice(index, 1);
      };

      $scope.addNewPrice = function () {
        $scope.pricesToAdd.push({
          format: '',
          description: '',
          price: ''
        });
      };

      ///main operation for the cinema
      $scope.save = function () {
        if ($scope.isNew) { //in case of new cinema
          CinemasFactory.save(null, $scope.cinemaData,
            function (response) {
              $state.go("cineBreak.specificCinema", {id: response._id, cinema: response});
            },
            function (err) {
              console.log("Error: " + err.status + " " + err.statusText);
            });
        } else {
          CinemasFactory.update({id: $stateParams.id}, $scope.cinemaData,
            function (cinema) {
              $state.go("cineBreak.specificCinema", {id: cinema._id, cinema: cinema});
            }, function (err) {
              console.log("Error: " + err.status + " " + err.statusText);
            });
        }
      };

      $scope.remove = function () {
        CinemasFactory.remove({id: $stateParams.id},
          function (data) {
            $state.go("cineBreak.cinemas");
          },
          function (err) {
            console.log("Error: " + err.status + " " + err.statusText);
          })
      };

      $scope.cancel = function () {
        if (!$scope.isNew) {
          $state.go("cineBreak.specificCinema", {id: $scope.cinemaData._id});
        } else {
          $state.go("cineBreak.cinemas");
        }
      }

    }]);
