'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:HomeController
 * @description
 * # HomeController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('MoviesController', ['$scope', '$rootScope', '$state', '$stateParams', 'MoviesFactory','AuthFactory',

    function ($scope, $rootScope, $state, $stateParams, MoviesFactory,AuthFactory) {
      $scope.movies = [];
      
      //operations for admins
      $scope.isAdmin = AuthFactory.isAdmin();

      $rootScope.$on('login:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('logout:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('registration:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      
      
      if ($stateParams.city) {

        MoviesFactory.query({'screenings._id.city.abbreviation': $stateParams.city},
          function (response) {
            $scope.movies = response;
          },
          function (response) {
            console.log("Error: " + response.status + " " + response.statusText);
          });
      }

      $scope.viewSpecificMovie = function(selectedMovie){
        $state.go('cineBreak.specificMovie', { id : selectedMovie._id, movie:selectedMovie });
      }

    }]);
