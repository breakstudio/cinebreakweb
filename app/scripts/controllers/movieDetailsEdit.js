/**
 * Created by Kevin on 08/09/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:MovieDetailsEditController
 * @description
 * # MovieDetailsController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('MovieDetailsEditController', ['$scope', '$modal', '$state', '$stateParams', 'commentFactory', 'AuthFactory', 'MoviesFactory', 'CinemasFactory',
    function ($scope, $modal, $state, $stateParams, commentFactory, AuthFactory, MoviesFactory, CinemasFactory) {

      //info of the cinemas for the screenings
      $scope.cinemas = [];
      CinemasFactory.query({'city.abbreviation': $stateParams.city},
        function (response) {
          $scope.cinemas = response;
        },
        function (response) {
          console.log("Error: " + response.status + " " + response.statusText);
        });


      //getting the information of the movie
      $scope.isNew = false;

      if ($stateParams.movie) {
        $scope.movie = $stateParams.movie;
      }
      else {//in case of new movie
        $scope.movie = {cast: [], screenings:[]};
        $scope.isNew = true;
      }

      //operations for the cast collection
      $scope.actorsToAdd = [{
        name: '',
        imdbID: ''
      }];

      $scope.addNewActorToCastArray = function (actorToAdd) {
        var index = $scope.actorsToAdd.indexOf(actorToAdd);
        $scope.actorsToAdd.splice(index, 1);
        $scope.movie.cast.push(angular.copy(actorToAdd))
      };

      $scope.removeActorFromCastArray = function (actorToRemove) {
        var index = $scope.movie.cast.indexOf(actorToRemove);
        $scope.movie.cast.splice(index, 1);
      };

      $scope.addNewActor = function () {
        $scope.actorsToAdd.push({
          firstName: '',
          lastName: ''
        });
      };


      //operations for the screenings collection
      $scope.screeningToAdd = {};

      $scope.screeningsToAdd = [{
        _id: ''
      }];

      $scope.addNewScreeningToScreeningsArray = function (screeningToAdd) {
        var index = $scope.screeningsToAdd.indexOf(screeningToAdd);
        $scope.screeningsToAdd.splice(index, 1);
        $scope.movie.screenings.push({
          _id: {
            _id: screeningToAdd._id._id,
            displayName: screeningToAdd._id.displayName,
            city: {
              abbreviation: screeningToAdd._id.city.abbreviation,
              name: screeningToAdd._id.city.name
            }
          },
          showtimes: []
        });

        console.log($scope.movie.screenings);

      };

      $scope.removeScreeningFromScreeningsArray = function (screeningToRemove) {
        var index = $scope.movie.screenings.indexOf(screeningToRemove);
        $scope.movie.screenings.splice(index, 1);
      };

      $scope.addNewScreening = function () {
        $scope.screeningsToAdd.push({
          _id: ''
        });
      };

      ///operations for showtimes of a specific screening
      /***
       * Opens the window for showtimes of a specific screening
       * @param screening
       * @param size how big the modal will be posible values can be: 'tiny', 'small', 'large', 'full'
       * @param backdrop
       * @param itemCount
       * @param closeOnClick
       */
      $scope.openShowtimesOfAScreening = function (screening, size, backdrop, itemCount, closeOnClick) {
        var params = {
          templateUrl: './views/showtimesOfAScreening.html',
          resolve: {},
          controller: function ($scope, $modalInstance) {

            $scope.screening = screening;

            ///operations for showtimes of a specific screening
            $scope.showtimesToAdd = [{
              format: '',
              type: '',
              time:''
            }];

            $scope.addNewShowtimeToShowtimesArray = function (showtimeToAdd) {
              var index = $scope.showtimesToAdd.indexOf(showtimeToAdd);
              $scope.showtimesToAdd.splice(index, 1);
              $scope.screening.showtimes.push(angular.copy(showtimeToAdd))
            };

            $scope.removeShowtimeFromShowtimesArray = function (showtimeToRemove) {
              var index = $scope.screening.showtimes.indexOf(showtimeToRemove);
              $scope.screening.showtimes.splice(index, 1);
            };

            $scope.addNewShowtime = function () {
              $scope.showtimesToAdd.push({
                firstName: '',
                lastName: ''
              });
            };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }
        };

        if (angular.isDefined(closeOnClick)) {
          params.closeOnClick = closeOnClick;
        }

        if (angular.isDefined(size)) {
          params.size = size;
        }

        if (angular.isDefined(backdrop)) {
          params.backdrop = backdrop;
        }

        var modalInstance = $modal.open(params);

        /***
         * When the user closes the CitySelector modal window
         */
        modalInstance.result.then(
          function (selectedOption) { //when closed after selecting a city option
            switch (selectedOption) {
              case 'aboutCinemas':
                $state.go('cineBreak.cinemas');
                break;
              case 'cartelera':
                $state.go('cineBreak');
                break;
            }

          }, function () { //when dismissed
          });
      };


      ///main operation for the movie
      $scope.save = function () {
        if ($scope.isNew) { //in case of new movie
          MoviesFactory.save(null, $scope.movie,
            function (response) {
              $state.go("cineBreak.specificMovie", {id: response._id, movie: response});
            },
            function (err) {
              console.log("Error: " + err.status + " " + err.statusText);
            });
        } else {
          MoviesFactory.update({id: $stateParams.id}, $scope.movie,
            function (movie) {
              $state.go("cineBreak.specificMovie", {id: movie._id, movie: movie});
            }, function (err) {
              console.log("Error: " + err.status + " " + err.statusText);
            });
        }
      };

      $scope.remove = function () {
        MoviesFactory.remove({id: $stateParams.id},
          function (data) {
            $state.go("cineBreak.movies");
          },
          function (err) {
            console.log("Error: " + err.status + " " + err.statusText);
          })
      };

      $scope.cancel = function () {
        if (!$scope.isNew) {
          $state.go("cineBreak.specificMovie", {id: $scope.movie._id});
        } else {
          $state.go("cineBreak.movies");
        }
      }


    }]);
