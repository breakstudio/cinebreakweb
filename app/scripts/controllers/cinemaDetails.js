/**
 * Created by Kevin on 08/09/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:CinemaDetailsController
 * @description
 * # MovieDetailsController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('CinemaDetailsController', ['$scope', '$rootScope', '$state', '$stateParams', 'NgMap', 'CinemasFactory', 'AuthFactory',
    function ($scope, $rootScope, $state, $stateParams, NgMap, CinemasFactory, AuthFactory) {
      $scope.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBjgG68AS6vo-Cf8SI3B4ooGJFOqUnub1U";
      $scope.cinemaId = $stateParams.id;

      //Enable the admin operations
      $scope.isAdmin = AuthFactory.isAdmin();

      $rootScope.$on('login:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('logout:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });
      $rootScope.$on('registration:Successful', function () {
        $scope.isAdmin = AuthFactory.isAdmin();
      });


      if ($stateParams.cinema) {
        $scope.cinema = $stateParams.cinema;
      }
      else {
        CinemasFactory.get({id: $scope.cinemaId},
          function (data) {
            $scope.cinema = data;
          },
          function (err) {
            $scope.message = "Error: " + err.status + " " + err.statusText;
          });
      }

      // NgMap.getMap().then(function (map) {
      //   // console.log(map.getCenter());
      //   // console.log('markers', map.markers);
      //   // console.log('shapes', map.shapes);
      // });
    }]);
