'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
