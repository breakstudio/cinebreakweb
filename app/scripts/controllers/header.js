'use strict';

/**
 * @ngdoc function
 * @name cinebreakwebApp.controller:HeaderController
 * @description
 * # HeaderController
 * Controller of the cinebreakwebApp
 */
angular.module('cinebreakwebApp')
  .controller('HeaderController', ['$scope', '$rootScope', '$modal', '$state', '$stateParams', 'AuthFactory',
    function ($scope, $rootScope, $modal, $state, $stateParams, AuthFactory) {

      $scope.loggedIn = AuthFactory.isAuthenticated();
      $scope.username = AuthFactory.getUsername();

      $scope.logOut = function () {
        AuthFactory.logout();
        $scope.loggedIn = false;
        $scope.username = '';
      };

      $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
      });

      $rootScope.$on('registration:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
      });


      /***
       * Opens the menu window
       * @param size how big the modal will be posible values can be: 'tiny', 'small', 'large', 'full'
       * @param backdrop
       * @param itemCount
       * @param closeOnClick
       */
      $scope.openMenu = function (size, backdrop, itemCount, closeOnClick) {
        var params = {
          templateUrl: './views/menu.html',
          resolve: {},
          controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {

            $scope.city = $stateParams.city;

            $scope.login = function () {

              /***
               * Opens the loginmodal window
               * @param size how big the modal will be posible values can be: 'tiny', 'small', 'large', 'full'
               * @param backdrop
               */
              $scope.openLogin = function (size, backdrop) {
                var params = {
                  templateUrl: './views/login.html',
                  resolve: {},
                  controller: ['$scope', '$modalInstance', '$localStorage', 'AuthFactory',
                    function ($scope, $modalInstance, $localStorage, AuthFactory) {
                      $scope.activeView = "login";

                      $scope.alerts = [];
                      $scope.alertsRegister = [];
                      $scope.loginData = $localStorage.getObject('userinfo', '{}');
                      $scope.registrationData = {};

                      $scope.showRegister = function () {
                        $scope.activeView = "register";
                      };
                      $scope.showLogin = function () {
                        $scope.activeView = "login";
                      };

                      $scope.doLogin = function () {
                        AuthFactory.login($scope.loginData,
                          function (successful, err) {
                            if (successful) {
                              $modalInstance.close("login");
                            } else {
                              $scope.alerts.push({type: 'alert', msg: err.name + ": " + err.message});
                            }
                          });
                      };

                      $scope.closeAlert = function (index) {
                        $scope.alerts.splice(index, 1);
                      };

                      $scope.closeRegisterAlert = function (index) {
                        $scope.alertsRegister.splice(index, 1);
                      };

                      $scope.doRegister = function () {
                        AuthFactory.register($scope.registrationData, function (successful, err) {
                          if (successful) {
                            $modalInstance.close("login");
                          } else {
                            $scope.alertsRegister.push({type: 'alert', msg: err.name + ": " + err.message});
                          }
                        });

                      };

                      $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                      };
                    }]
                };

                params.closeOnClick = false;

                if (angular.isDefined(size)) {
                  params.size = size;
                }

                if (angular.isDefined(backdrop)) {
                  params.backdrop = backdrop;
                }

                var loginModalInstance = $modal.open(params);

                /***
                 * When the user closes the login modal window
                 */
                loginModalInstance.result.then(
                  function (user) { //when closed after selecting a city option
                    console.log(user);
                    // $state.go('cineBreak',
                    //   {
                    //     city: selectedCity.abbreviation
                    //   });
                  }, function () { //when dismissed
                  });
              };

              $scope.openLogin('small');

              //closes the menu modal
              $modalInstance.close('login');
            };

            $scope.cartelera = function () {
              $modalInstance.close('movies');
            };

            $scope.aboutCinemas = function () {
              $modalInstance.close('aboutCinemas');
            };

            $scope.howDoesItWork = function () {
              //$scope.selectedCityAbbreviation = city;
              //$modalInstance.close($scope.selectedCityAbbreviation);
            };

            $scope.aboutTheApp = function () {
              //$scope.selectedCityAbbreviation = city;
              //$modalInstance.close($scope.selectedCityAbbreviation);
            };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }]
        };

        if (angular.isDefined(closeOnClick)) {
          params.closeOnClick = closeOnClick;
        }

        if (angular.isDefined(size)) {
          params.size = size;
        }

        if (angular.isDefined(backdrop)) {
          params.backdrop = backdrop;
        }

        var modalInstance = $modal.open(params);

        /***
         * When the user closes the CitySelector modal window
         */
        modalInstance.result.then(
          function (selectedOption) { //when closed after selecting a city option
            switch (selectedOption) {
              case 'aboutCinemas':
                $state.go('cineBreak.cinemas');
                break;
              case 'movies':
                $state.go('cineBreak');
                break;
            }

          }, function () { //when dismissed
          });
      };


      /***
       * Opens the CitySelectormodal window
       * @param size how big the modal will be posible values can be: 'tiny', 'small', 'large', 'full'
       * @param backdrop
       * @param itemCount
       * @param closeOnClick
       */
      $scope.openCitySelector = function (size, backdrop, itemCount, closeOnClick) {
        var params = {
          templateUrl: './views/citySelector.html',
          resolve: {
            cities: ['CitiesFactory', function (CitiesFactory) {
              return CitiesFactory;
            }]
          },
          controller: ['$scope', '$modalInstance', 'cities', function ($scope, $modalInstance, cities) {
            cities.query(
              function (response) {
                $scope.cities = response;
              },
              function (response) {
                console.log("Error: " + response.status + " " + response.statusText);
              });

            $scope.selectedCityAbbreviation = null;

            $scope.select = function (city) {
              $scope.selectedCityAbbreviation = city;
              $modalInstance.close($scope.selectedCityAbbreviation);
            };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }]
        };

        if (angular.isDefined(closeOnClick)) {
          params.closeOnClick = closeOnClick;
        }

        if (angular.isDefined(size)) {
          params.size = size;
        }

        if (angular.isDefined(backdrop)) {
          params.backdrop = backdrop;
        }

        var modalInstance = $modal.open(params);

        /***
         * When the user closes the CitySelector modal window
         */
        modalInstance.result.then(
          function (selectedCity) { //when closed after selecting a city option
            $state.go('cineBreak',
              {
                city: selectedCity.abbreviation
              });
          }, function () { //when dismissed
          });
      };


      /*
       If the user didn't specify a city in the url must select it from the city Selector
       */
      if ($stateParams.city) {
        $scope.selectedCityAbbreviation = $stateParams.city;
      } else {
        $scope.openCitySelector('full');
        $scope.selectedCityAbbreviation = "Select City";
      }
    }]);
