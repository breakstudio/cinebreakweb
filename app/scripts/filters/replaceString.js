/**
 * Created by Kevin on 19/09/2016.
 */
angular.module('cinebreakwebApp')
  .filter('replaceString', function () {
    return function (input, oldStr, newStr) {
      return input.replace(oldStr, newStr);
    };
  });
