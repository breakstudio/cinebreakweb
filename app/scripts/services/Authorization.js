/**
 * Manages all the security
 */
angular.module('cinebreakwebApp')
  .factory('AuthFactory', ['$resource', '$http', '$localStorage', '$rootScope', '$window', 'webAPIURL', 'jwtHelper',
    function ($resource, $http, $localStorage, $rootScope, $window, webAPIURL, jwtHelper) {

      var authFac = {};
      var TOKEN_KEY = 'userinfo';
      var isAuthenticated = false;
      var username = '';
      var authToken = undefined;
      var isAdmin = false;


      function loadUserCredentials() {
        var credentials = $localStorage.getObject(TOKEN_KEY, '{}');
        if (credentials.username != undefined) {
          useCredentials(credentials);
        }
      }

      function storeUserCredentials(credentials) {
        $localStorage.storeObject(TOKEN_KEY, credentials);
        useCredentials(credentials);
      }

      function useCredentials(credentials) {
        isAuthenticated = true;
        username = credentials.username;
        authToken = credentials.token;

        var tokenPayload = jwtHelper.decodeToken(authToken);
        isAdmin = tokenPayload.admin;

        // Set the token as header for your requests!
        $http.defaults.headers.common['x-access-token'] = authToken;
      }

      function destroyUserCredentials() {
        authToken = undefined;
        username = '';
        isAuthenticated = false;
        isAdmin = false;
        $http.defaults.headers.common['x-access-token'] = authToken;
        $localStorage.remove(TOKEN_KEY);
      }

      authFac.login = function (loginData, callback) {

        $resource(webAPIURL + "users/login")
          .save(loginData,
            function (response) {
              storeUserCredentials({username: loginData.username, token: response.token});
              $rootScope.$broadcast('login:Successful');
              callback(true, null);
            },
            function (response) {
              isAuthenticated = false;
              callback(false, response.data.err);
            }
          );

      };

      authFac.logout = function () {
        $resource(webAPIURL + "users/logout").get(function (response) {
        });
        destroyUserCredentials();
        $rootScope.$broadcast('logout:Successful');
      };

      authFac.register = function (registerData, callback) {

        $resource(webAPIURL + "users/register")
          .save(registerData,
            function (response) {
              authFac.login({username: registerData.username, password: registerData.password});
              if (registerData.rememberMe) {
                $localStorage.storeObject('userinfo',
                  {username: registerData.username, password: registerData.password});
              }

              $rootScope.$broadcast('registration:Successful');
              callback(true, null);
            },
            function (response) {
              callback(false, response.data.err);
            }
          );
      };

      authFac.isAuthenticated = function () {
        return isAuthenticated;
      };

      authFac.getUsername = function () {
        return username;
      };

      authFac.isAdmin = function () {
        return isAdmin;
      }

      loadUserCredentials();

      return authFac;

    }]);
