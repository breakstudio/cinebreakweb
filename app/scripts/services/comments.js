/**
 * Created by Kevin on 22/09/2016.
 */
angular.module('cinebreakwebApp')
  .constant("webAPIURL", "http://breakstudio.co:8080/")

  .factory('commentFactory', ['$resource', 'webAPIURL', function ($resource, webAPIURL) {

    return $resource(webAPIURL + "movies/:id/comments/:commentId", {id: "@Id", commentId: "@CommentId"}, {
      'update': {
        method: 'PUT'
      }
    });

  }]);
