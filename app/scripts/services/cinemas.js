/**
 * Created by Kevin on 17/09/2016.
 */

angular.module('cinebreakwebApp')
  //.constant("webAPIURL", "http://localhost:3000/")

  .factory('CinemasFactory', ['$resource', "webAPIURL",
    function ($resource, webAPIURL) {

      return $resource(webAPIURL + "cinemas/:id", {id: "@id"},
        {
          'query': {method: 'GET', isArray: true},
          'update': {method: 'PUT'},
          'remove': {method: 'DELETE'}
        });

    }]);
