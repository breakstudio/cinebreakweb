/**
 * Created by Kevin on 17/09/2016.
 */

angular.module('cinebreakwebApp')
  //.constant("webAPIURL", "http://localhost:3000/")

  .factory('CitiesFactory', ['$resource', "webAPIURL",
    function ($resource, webAPIURL) {

      return $resource(webAPIURL + "cities/", null, {
        'query': {method: 'GET', isArray: true}
      });

    }]);
