/**
 * Created by Kevin on 17/09/2016.
 */

angular.module('cinebreakwebApp')

  .factory('MoviesFactory', ['$resource', "webAPIURL",
    function ($resource, webAPIURL) {

      return $resource(webAPIURL + "movies/:id", {id: "@id"},
        {
          'query': {method: 'GET', isArray: true},
          'update': {method: 'PUT'},
          'remove': {method: 'DELETE'}
        });

    }]);
