'use strict';

/**
 * @ngdoc overview
 * @name cinebreakwebApp
 * @description
 * # cinebreakwebApp
 *
 * Main module of the application.
 */
angular.module('cinebreakwebApp', ['ui.router', 'ngCookies', 'ngSanitize', 'ngResource',
    'mm.foundation', 'mm.foundation.offcanvas', 'ngMap', 'angular.filter', 'angular-jwt'])
  .run(function ($rootScope) {
    //$rootScope.$apply($(document).foundation());
  })
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('cineBreak', {
        url: '/:city',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderController'
          },
          'content': {
            templateUrl: 'views/movies.html',
            controller: 'MoviesController'
          }
        }
      })
      .state('cineBreak.new', {
        url: '/movies/new',
        views: {
          'content@': {
            templateUrl: 'views/movies.html',
            controller: 'MoviesController'
          },
          'specificItem@': {
            templateUrl: 'views/movieDetailsEdit.html',
            controller: 'MovieDetailsEditController'
          }
        }
      })
      .state('cineBreak.specificMovie', {
        url: '/movies/:id',
        params: {movie: null},
        views: {
          'content': {
            templateUrl: 'views/movies.html',
            controller: 'MoviesController'
          },
          'specificItem@': {
            templateUrl: 'views/movieDetails.html',
            controller: 'MovieDetailsController'
          }
        }
      })
      .state('cineBreak.specificMovie.edit', {
        url: '/edit',
        params: {movie: null},
        views: {
          'content': {
            templateUrl: 'views/movies.html',
            controller: 'MoviesController'
          },
          'specificItem@': {
            templateUrl: 'views/movieDetailsEdit.html',
            controller: 'MovieDetailsEditController'
          }
        }
      })
      .state('cineBreak.cinemas', {
        url: '/cinemas',
        views: {
          'content@': {
            templateUrl: 'views/cinemas.html',
            controller: 'CinemasController'
          }
        }
      })
      .state('cineBreak.cinemas.new', {
        url: '/cinemas/new',
        views: {
          'content@': {
            templateUrl: 'views/cinemas.html',
            controller: 'CinemasController'
          },
          'specificItem@': {
            templateUrl: 'views/cinemaDetailsEdit.html',
            controller: 'CinemaDetailsEditController'
          }
        }
      })
      .state('cineBreak.specificCinema', {
        url: '/cinemas/:id',
        params: {cinema: null},
        views: {
          'content@': {
            templateUrl: 'views/cinemas.html',
            controller: 'CinemasController'
          },
          'specificItem@': {
            templateUrl: 'views/cinemaDetails.html',
            controller: 'CinemaDetailsController'
          }
        }
      })
      .state('cineBreak.specificCinema.edit', {
        url: '/edit',
        params: {cinema: null},
        views: {
          'content@': {
            templateUrl: 'views/cinemas.html',
            controller: 'CinemasController'
          },
          'specificItem@': {
            templateUrl: 'views/cinemaDetailsEdit.html',
            controller: 'CinemaDetailsEditController'
          }
        }
      })      ;

    $urlRouterProvider.otherwise('/');
  });

